import { UIDriver } from "../Types.d.ts";

export class RealtimeUIDriver implements UIDriver {
  public printLine(...messages: Array<string>): void {
    console.log(messages.join(" "));
  }

  public pause(): void {
    prompt("");
  }

  public clear(): void {
    console.clear();
  }
}
