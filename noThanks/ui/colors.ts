export const COLOR_BLACK = 30;

const bold = 1;

const black = 30;
const red = 31;
const green = 32;
const yellow = 33;
const blue = 34;
const magenta = 35;
const cyan = 36;
const white = 37;

const bgBlack = 40;
const bgRed = 41;
const bgGreen = 42;
const bgYellow = 43;
const bgBlue = 44;
const bgMagenta = 45;
const bgCyan = 46;
const bgWhite = 47;

function color(str: string, color: number) {
  return "\x1b[" + color.toString() + "m" + str + "\x1b[0m";
}

export function colorBold(str: string) {
  return color(str, 1);
}

export function colorCyan(str: string) {
  return color(str, 46);
}

export function colorActive(str: string) {
  return "\x1b[30m\x1b[47m" + str + "\x1b[0m";
}

export function colorScoringCard(str: string) {
  return color(str, yellow);
}

export function colorUnscoringCard(str: string) {
  return color(str, green);
}
export function colorRelevantCard(str: string) {
  return color(str, cyan);
}

export function colorYes(str: string) {
  return color(str, bgGreen);
}

export function colorNo(str: string) {
  return color(str, bgRed);
}
