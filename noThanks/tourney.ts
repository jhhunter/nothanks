import Game, { GameSeatResult } from "./Game.ts";
import { Bot, BotFactory } from "./Types.d.ts";
import { RealtimeUIDriver } from "./ui/RealtimeUIDriver.ts";
import { ReplayUIDriver } from "./ui/ReplayUIDriver.ts";
import { Shuffle } from "./Utilities.ts";
import { combinations } from "https://deno.land/x/combinatorics@1.1.2/combinations.ts";

interface Contestant {
  name: string;
  factory: BotFactory;
  id: number;
  points: number;
}

export interface GameReplay {
  finishingOrder: Array<GameSeatResult>;
  replay: ReplayUIDriver;
}

/** Tourney code */
export function runTourney(
  entries: Array<BotFactory>,
  pointScheme: Array<number>,
  minGameCount: number,
  idealPlayersPerGame: number,
) {
  const ui = new RealtimeUIDriver();
  const contestants = createContestants(entries);
  let roundCount = 0;
  let gameCount = 0;
  const history: Array<GameReplay> = [];

  // These are the different player groups that can play together
  const contestantLineups = [
    ...combinations(contestants, idealPlayersPerGame),
    ...combinations(contestants, idealPlayersPerGame - 1),
    ...combinations(contestants, idealPlayersPerGame + 1),
  ];
  const gamesPerLineup = Math.ceil(minGameCount / contestantLineups.length);

  console.clear();
  console.log("Please welcome our", contestants.length, "bots!");
  console.log();
  contestants.forEach((c) => {
    prompt(c.name);
  });

  console.log();
  prompt("** Applause **");

  console.log(
    "Found",
    contestantLineups.length,
    "different combinations of bots.",
  );
  console.log("Running", gamesPerLineup, "games per combination");

  // Run games until there are no ties
  while (roundCount++ < gamesPerLineup || detectTies(contestants)) {
    // Run one game for each lineup
    contestantLineups.forEach((lineup) => {
      gameCount++;
      const replay = new ReplayUIDriver();

      const game = new Game(replay);
      const results = game.play(getPlayers(lineup));

      history.push({
        finishingOrder: results.slice(),
        replay: replay,
      });

      rewardContestants(contestants, results, pointScheme);
    });
  }

  const finalTourneyOrder = contestants.slice();
  finalTourneyOrder.sort((a, b) => b.points - a.points);

  // Organize the contestants into groups
  // We will watch one game from each group
  // Starting with the losers game
  const replayGroups = segmentArray(finalTourneyOrder);

  console.log(
    "Finished running",
    contestantLineups.length * gamesPerLineup,
    "games",
  );
  ui.printLine("Results have been determined.");

  if (replayGroups.length > 0) {
    ui.printLine(
      "We will show " + replayGroups.length +
        " game replays, starting with the worst performers",
    );
  }

  // ui.pause();

  for (let i = replayGroups.length - 1; i >= 0; i--) {
    let replay: ReplayUIDriver | undefined;

    ui.clear();

    if (i === 0) {
      ui.printLine("***Championship Game***");
    } else {
      ui.printLine("Game " + (replayGroups.length - i));
    }

    // See if we already played a game that would
    // perfectly demonstrate the reletive strength of each bot
    replay = findPerfectReplay(
      replayGroups[i].map((c) => c.id),
      history,
    )?.replay as ReplayUIDriver;

    // We must play games until we find a good example
    if (!replay) {
      ui.printLine("Playing games until the game finishes in tourney order...");
      replay = generatePerfectReplay(replayGroups[i]);
      ui.printLine("Done!");
    }

    // ui.pause();

    // Play the example
    // replay?.replay(ui);
    // ui.pause();
  }

  // Print tourney results
  displayStandings(contestants);
}

/** Print the current standings in the tournament */
function displayStandings(contestants: Array<Contestant>) {
  const standings = contestants.slice();
  standings.sort((a, b) => b.points - a.points);

  console.clear();

  console.log("*** Tournament Results ***");
  console.log();

  for (let i = 0; i < standings.length; i++) {
    console.log(standings[i].name, standings[i].points);
  }
}

/**
 * Reward points based on the results of a single match.
 * Modifies the contestants in the passed-in array.
 */
function rewardContestants(
  contestants: Array<Contestant>,
  results: Array<GameSeatResult>,
  pointScheme: Array<number>,
) {
  for (let i = 0; i < Math.min(results.length, pointScheme.length); i++) {
    contestants[results[i].id].points += pointScheme[i];
  }
}

/**
 * Create a contestant object for each Bot constructor
 */
function createContestants(entries: Array<BotFactory>): Array<Contestant> {
  return entries.map((e, index) => ({
    name: new e(0).name,
    factory: e,
    id: index,
    points: 0,
  }));
}

/**
 * Get a list of bots in random order from the given contestants
 */
function getPlayers(contestants: Array<Contestant>): Array<Bot> {
  return Shuffle(contestants.map((c) => new c.factory(c.id)));
}

/**
 * Returns true of the array has two ore more contestants
 * with the same number of points
 */
function detectTies(contestants: Array<Contestant>): boolean {
  return detectDuplicates(contestants.map((c) => c.points));
}

/**
 * Returns true if the array has any duplicate entries
 */
export function detectDuplicates<T>(list: Array<T>): boolean {
  return new Set<T>(list).size < list.length;
}

/**
 * Out of the replays given, return one whose result
 * matches the given list of player ids
 */
export function findPerfectReplay(
  target: Array<number>,
  replays: Array<GameReplay>,
) {
  const targetJson = JSON.stringify(target);
  const matches = replays.filter((r) =>
    JSON.stringify(r.finishingOrder.map((p) => p.id)) === targetJson
  );

  console.log(
    "During the tournament, games between these bots ended with the bots in their tourney order",
    matches.length,
    "times.",
  );

  if (matches.length > 0) {
    return matches[0];
  } else {
    return undefined;
  }
}

/**
 * Run a bunch of games until we get the desired result.
 * Don't use this to cheat!
 *
 * @param players The players for the game, in the correct finishing
 * order, but they should be scrambled on the table in the games
 * @returns replay of the game where the right person wins.
 */
function generatePerfectReplay(
  players: Array<Contestant>,
) {
  const target = players.map((p) => p.id);

  while (1) {
    const replay = new ReplayUIDriver();
    const game = new Game(replay);
    const results = game.play(getPlayers(players));

    const resultPlayerIds = results.map((r) => r.id);
    const resultScores = results.map((r) => r.score);

    if (
      JSON.stringify(resultPlayerIds) === JSON.stringify(target) &&
      !detectDuplicates(resultScores)
    ) {
      return replay;
    }
  }
}

/**
 * Split an array into an array of arrays such that
 * each subArray is of length 3, 4, or 5.
 * Array of length 4 should be first, and
 * Array of length 5 should be last
 */
export function segmentArray<T>(list: Array<T>): Array<Array<T>> {
  const input = list.slice();
  const outcome = [];

  while (input.length > 0) {
    let choose = 4;

    if (input.length < 6) {
      choose = input.length;
    } else if (input.length === 6) {
      choose = 3;
    }

    outcome.push(input.splice(0, choose));
  }

  return outcome;
}
