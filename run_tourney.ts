import BasicBot from "./bots/BasicBot.ts";
import JohnBot from "./bots/JohnBot.ts";
import JohnBot2 from "./bots/JohnBot2.ts";
import QuickBot from "./bots/QuickBot.ts";
import { runTourney } from "./noThanks/tourney.ts";

/*** Tourney Settings ***/
const entries = [
  JohnBot,
  JohnBot2,
  JohnBot,
  JohnBot2,
  JohnBot,
  JohnBot2,
  JohnBot,
  JohnBot2,
];

// Points awarded to each player depending on their finishing place.
// Players beyond those defined receive zero points.
const pointScheme = [
  7,
  4,
  2,
  1,
];

const minGameCount = 2000;

const idealPlayersPerGame = 4;

runTourney(entries, pointScheme, minGameCount, idealPlayersPerGame);
